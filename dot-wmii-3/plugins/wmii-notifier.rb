Plugin.define "notifier" do
  author '"Sridhar Ratnakumar" <sridhar.ratna@gmail.com>'
  bar_applet("notifier", 450) do |wmii, bar|
    Thread.new do
      interval = wmii.plugin_config["notifier:notifier"]["interval"] || 1
      loop do
        bar.data = `cat ~/.wmii-notifier`
        sleep interval
      end
    end
  end
end


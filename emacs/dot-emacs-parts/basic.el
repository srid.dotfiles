
;; Default settings
(setq inhibit-startup-message t)
(setq backup-inhibited t)
(fset 'yes-or-no-p 'y-or-n-p)
(set-default 'fill-column 80)
(setq frame-title-format "emacs")


;; org
(require 'org-install)
(add-to-list 'auto-mode-alist '("\\.org$" . org-mode))
(setq org-log-done t)
(global-set-key (kbd "C-c C-g") 'org-agenda-list)
(setq org-agenda-files (directory-files "~/org/" t ".*\\.org$"))

;; -----
;; irc
;; -----
(load "~/.freenode.el")
(setq rcirc-authinfo
	  (list (list "freenode" 'nickserv "srid" passwd)
			(list "oftc"     'nickserv "srid" passwd)
			(list "bitlbee"  'bitlbee  "srid" passwd)))

(defun im ()
  (interactive)
  (rcirc-connect "im.bitlbee.org" 6667
				 "srid" "srid" "Sridhar Ratnakumar"))

(defun switch-to-im ()
  (interactive)
  (let ((im-buffer (get-buffer "&bitlbee@im.bitlbee.org")))
	(when im-buffer
	  (switch-to-buffer im-buffer)
	  (rcirc-cmd-names "&bitlbee"))))
(global-set-key (kbd "C-<f11>") 'switch-to-im)

(defun irc-count-nicks ()
  (interactive)
  (message
   (format "%d"
		   (length (rcirc-channel-nicks
					(rcirc-buffer-process)
					rcirc-target)))))

(setq notify-wav (concat datadir "notify.wav"))

(defun rcirc-alert (p sender resp target txt)
  (when (and (string-match (rcirc-nick p) txt)
			 (not (string= (rcirc-nick p) sender))
			 (not (string= (rcirc-server-name p) sender)))
	(play-sound-file notify-wav)))

; (add-hook 'rcirc-print-hooks 'rcirc-alert)

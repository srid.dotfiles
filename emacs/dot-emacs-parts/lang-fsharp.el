;; Tuareg mode for F#
;
(add-to-list 'load-path (concat extdir "tuareg"))

(require 'tuareg)
(setq auto-mode-alist (cons '("\\.fs\\w?" . tuareg-mode) auto-mode-alist))

(add-hook 'tuareg-mode-hook
		  '(lambda ()
			 (set (make-local-variable 'compile-command)
				  (concat "fsc \""
						  (file-name-nondirectory buffer-file-name)
						  "\""))))

(defadvice tuareg-find-alternate-file (around fsharp-find-alternate-file)
  "Switch Implementation/Interface."
  (interactive)
  (let ((name (buffer-file-name)))
	(if (string-match "\\`\\(.*\\)\\.fs\\(i\\)?\\'" name)
		(find-file (concat (tuareg-match-string 1 name)
						   (if (match-beginning 2) ".fs" ".fsi"))))))

(defvar tuareg-interactive-program "fsi"
  "*Default program name for invoking a FSharp toplevel from Emacs.")

(defconst tuareg-error-regexp-fs
  "^\\([^(\n]+\\)(\\([0-9]+\\),\\([0-9]+\\)):"
  "Regular expression matching the error messages produced by fsc.")

(add-hook 'tuareg-mode-hook
		  '(lambda ()
			 (ad-activate 'tuareg-find-alternate-file)
			 (setq tuareg-interactive-program "fsi")
			 (if (boundp 'compilation-error-regexp-alist)
				 (or (assoc tuareg-error-regexp-fs
							compilation-error-regexp-alist)
					 (setq compilation-error-regexp-alist
						   (cons (list tuareg-error-regexp-fs 1 2 3)
								 compilation-error-regexp-alist))))))

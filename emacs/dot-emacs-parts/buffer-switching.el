;; Buffer switching
(require 'ido)
(ido-mode t)
(setq ido-enable-flex-matching t)

(global-set-key (kbd "<f11>") 'ido-switch-buffer)
(global-set-key (kbd "<f9>") 'ido-switch-buffer) ;; for gnome-terminal fullscreen conflict


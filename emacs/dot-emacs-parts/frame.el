;; frame manipulation functions

(defun create-frame (name)
  "Create a new frame with given name applying any fixes"
  (interactive "sFrame name: ")
  (let ((f (new-frame)))
	(select-frame f)
	(set-frame-name name)
	(my-set-font)
	f))


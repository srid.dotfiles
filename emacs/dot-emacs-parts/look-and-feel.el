
;(setq font-consolas-pt9
;      (concat "-outline-Consolas-normal-r-normal-normal"
;	      "-12-90-96-96-c-*-iso8859-1"))

(setq font-monaco
      (concat "-outline-Monaco-normal-r-*-*-11-*-96-96-c-*-iso8859-2"))

(defun my-set-font ()
 (if (equal system-type 'windows-nt)
	 (set-face-font 'default font-monaco)
	 (set-default-font "Monospace-8")))

(my-set-font)

(require 'color-theme)
(load "color-theme-library.el")
(color-theme-whateveryouwant)



;; mouse wheell
(mwheel-install)
(xterm-mouse-mode 1)
(mouse-wheel-mode 1)
(global-set-key [mouse-4] '(lambda ()
							 (interactive)
							 (scroll-down 1)))
(global-set-key [mouse-5] '(lambda ()
							 (interactive)
							 (scroll-up 1)))

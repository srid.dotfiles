;; functions to open several functionalities of emacs in seperate frame

(defun open-irc ()
  (interactive)
  (create-frame "irc")
  (irc nil))

(defun open-diary ()
  (interactive)
  (create-frame "diary")
  (calendar)
  (diary-mark-entries))

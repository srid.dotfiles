;; vbscript mode from http://www.emacswiki.org/cgi-bin/wiki/visual-basic-mode.el

(autoload 'visual-basic-mode "visual-basic-mode" "Visual Basic mode." t)
(setq auto-mode-alist (append '(("\\.\\(frm\\|bas\\|cls\\|vbs\\)$" .
								 visual-basic-mode)) auto-mode-alist))

;ELC   
;;; Compiled by shoh@OHAMA-HOME on Wed Dec 15 12:16:14 2004
;;; from file d:/pkgs/emacs-21.3/site-lisp/camldebug.el
;;; in Emacs version 21.3.1
;;; with bytecomp version 2.85.4.1
;;; with all optimizations.

;;; This file uses dynamic docstrings, first added in Emacs 19.29.
(if (and (boundp 'emacs-version)
	 (< (aref emacs-version (1- (length emacs-version))) ?A)
	 (or (and (boundp 'epoch::version) epoch::version)
	     (string-lessp emacs-version "19.29")))
    (error "`camldebug.el' was compiled for Emacs 19.29 or later"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(byte-code "\302\303!\210\302\304!\210\302\305!\210\302\306!\210\302\307!\210\310B\311B\301B\312\301!\204( \313\314B\315B\313\207" [current-load-list camldebug-filter-accumulator require comint shell caml derived thingatpt camldebug-last-frame camldebug-delete-prompt-marker boundp nil camldebug-last-frame-displayed-p camldebug-filter-function] 2)
#@50 A regexp to recognize the prompt for ocamldebug.
(defvar camldebug-prompt-pattern "^(ocd) *" (#$ . 969))
#@43 Overlay for displaying the current event.
(defvar camldebug-overlay-event nil (#$ . 1080))
#@43 Overlay for displaying the current event.
(defvar camldebug-overlay-under nil (#$ . 1177))
#@42 Marker for displaying the current event.
(defvar camldebug-event-marker nil (#$ . 1274))
#@71 *If non-nil, always display current frame position in another window.
(defvar camldebug-track-frame t (#$ . -1369))
(byte-code "\203: \306\307!\210\306\310!\210\311\307!\204 \312\307!\210\311\310!\204! \313\310\314\"\210\315\316\211\"\317	\320\307#\210\315\316\211\"\317\n\320\310#\210\202? \321 \322\323B\324\323!\204M \325 \326B\324\326!\204] \327\330\331\"\332B\333\332\331\"\210\334\335\336\337#\207" [window-system camldebug-overlay-event camldebug-overlay-under camldebug-event-marker overlay-arrow-string current-load-list make-face camldebug-event camldebug-underline face-differs-from-default-p invert-face set-face-underline-p t make-overlay 1 overlay-put face make-marker "=>" camldebug-mode-map boundp make-sparse-keymap camldebug-mode-syntax-table make-char-table syntax-table nil camldebug-mode-abbrev-table define-abbrev-table put camldebug-mode derived-mode-parent comint-mode] 4)
#@918 Major mode for interacting with an inferior Camldebug process.

The following commands are available:

\{camldebug-mode-map}

\[camldebug-display-frame] displays in the other window
the last line referred to in the camldebug buffer.

\[camldebug-step], \[camldebug-back] and \[camldebug-next], in the camldebug window,
call camldebug to step, backstep or next and then update the other window
with the current file and position.

If you are in a source file, you may select a point to break
at, by doing \[camldebug-break].

Commands:
Many commands are inherited from comint mode.
Additionally we have:

\[camldebug-display-frame] display frames file in other window
\[camldebug-step] advance one line in program
C-x SPACE sets break point at current line.

In addition to any hooks its parent mode `comint-mode' might have run,
this mode runs the hook `camldebug-mode-hook', as the final step
during initialization.
(defalias 'camldebug-mode #[nil "\306 \210\306\307N\203 \310\311\307\312#\210\311\313\314\n!\204 \315\n\316 \"\210\317!\211\203. \f\320 =\2034 \321\322 \"\210)\203> \323\324\"\210\325\n!\210\326!\210#\327\330\331\"\210\332$\333 %\334&\335'()\336*B*)+\312,\330\337!\210\312\340-\341\342!\207" [major-mode mode-name camldebug-mode-map camldebug-mode-syntax-table parent local-abbrev-table comint-mode special put camldebug-mode t "Inferior CDB" keymap-parent set-keymap-parent current-local-map char-table-parent standard-syntax-table set-char-table-parent syntax-table mapatoms #[(symbol) "\302\303!	\"\206 \304	\303!JK$\207" [symbol camldebug-mode-abbrev-table intern-soft symbol-name define-abbrev] 5] use-local-map set-syntax-table mapcar make-local-variable (camldebug-last-frame-displayed-p camldebug-last-frame camldebug-delete-prompt-marker camldebug-filter-function camldebug-filter-accumulator paragraph-start) nil make-marker "" camldebug-marker-filter camldebug-complete shell-dirtrackp shell-directory-tracker run-hooks camldebug-mode-hook camldebug-mode-abbrev-table camldebug-last-frame camldebug-delete-prompt-marker camldebug-filter-accumulator camldebug-filter-function camldebug-prompt-pattern comint-prompt-regexp comint-dynamic-complete-functions paragraph-start camldebug-last-frame-displayed-p comint-input-sentinel] 5 (#$ . 2289) nil])
(defalias 'camldebug-numeric-arg #[(arg) "\205 \301!\207" [arg prefix-numeric-value] 2])
#@988 Define camldebug-NAME to be a command sending NAME ARGS and bound
to KEY, with optional doc string DOC.  Certain %-escapes in ARGS are
interpreted specially if present.  These are:

  %m    module name of current module.
  %d    directory of current source file.
  %c    number of current character position
  %e    text of the caml variable surrounding point.

  The `current' source file is the file of the current buffer (if
we're in a caml buffer) or the source file current at the last break
or step (if we're in the camldebug buffer), and the `current' module
name is the filename stripped of any *.ml* suffixes (this assumes the
usual correspondence between module and file naming is observed).  The
`current' position is that of the current buffer (if we're in a source
file) or the position of the last break or step (if we're in the
camldebug buffer).

If a numeric is present, it overrides any ARGS flags and its string
representation is simply concatenated with the COMMAND.
(defalias 'def-camldebug '(macro . #[(name key &optional doc args) "\305\306\307\"!\310\n\205 \311	\312\n\313\314\315F\257\316\317\320\fP\321	DF\316\322\323\fP\321	DFF)\207" [name fun doc args key intern format "camldebug-%s" progn defun (arg) (interactive "P") camldebug-call (camldebug-numeric-arg arg) define-key camldebug-mode-map "" quote caml-mode-map ""] 10 (#$ . 4694)]))
#@25 Step one event forward.
(defalias 'camldebug-step #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "step" nil camldebug-numeric-arg] 5 (#$ . 6076) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-step ""] 4)
#@26 Step one event backward.
(defalias 'camldebug-backstep #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "backstep" nil camldebug-numeric-arg] 5 (#$ . 6365) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-backstep ""] 4)
#@18 Run the program.
(defalias 'camldebug-run #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "run" nil camldebug-numeric-arg] 5 (#$ . 6667) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-run ""] 4)
#@29 Run the program in reverse.
(defalias 'camldebug-reverse #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "reverse" nil camldebug-numeric-arg] 5 (#$ . 6946) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-reverse ""] 4)
#@41 Go to latest time in execution history.
(defalias 'camldebug-last #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "last" nil camldebug-numeric-arg] 5 (#$ . 7248) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "\f" camldebug-last "\f"] 4)
#@23 Print the call stack.
(defalias 'camldebug-backtrace #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "backtrace" nil camldebug-numeric-arg] 5 (#$ . 7555) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-backtrace ""] 4)
#@36 Finish executing current function.
(defalias 'camldebug-finish #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "finish" nil camldebug-numeric-arg] 5 (#$ . 7857) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-finish ""] 4)
#@33 Print value of symbol at point.
(defalias 'camldebug-print #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "print" "%e" camldebug-numeric-arg] 5 (#$ . 8163) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-print ""] 4)
#@35 Display value of symbol at point.
(defalias 'camldebug-display #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "display" "%e" camldebug-numeric-arg] 5 (#$ . 8464) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-display ""] 4)
#@41 Step one event forward (skip functions)
(defalias 'camldebug-next #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "next" nil camldebug-numeric-arg] 5 (#$ . 8773) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-next ""] 4)
#@49 Go up N stack frames (numeric arg) with display
(defalias 'camldebug-up #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "up" nil camldebug-numeric-arg] 5 (#$ . 9078) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "<" camldebug-up "<"] 4)
#@51 Go down N stack frames (numeric arg) with display
(defalias 'camldebug-down #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "down" nil camldebug-numeric-arg] 5 (#$ . 9385) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key ">" camldebug-down ">"] 4)
#@33 Set breakpoint at current line.
(defalias 'camldebug-break #[(arg) "\301\302\303\304!#\207" [arg camldebug-call "break" "@ \"%m\" # %c" camldebug-numeric-arg] 5 (#$ . 9700) "P"])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-break ""] 4)
#@35 Display value of $NNN clicked on.
(defalias 'camldebug-mouse-display #[(click) "\211A@)\211@\nA@\306\212\307!\210\fb\210\310\311!\312\313\"\205' \314\315\"-\207" [click event start window pos symb nil select-window thing-at-point symbol string-match "^\\$[0-9]+$" camldebug-call "display"] 4 (#$ . 10012) "e"])
(define-key camldebug-mode-map [mouse-2] 'camldebug-mouse-display)
(defalias 'camldebug-kill-filter #[(string) "	P\305\306\"\203 \307\310\311\"B\312\305\"\203/ \313\224\314O\314\313\211\224SOB\312\f)\207\312\207" [camldebug-filter-accumulator string camldebug-kill-output comint-prompt-regexp output string-match "\\(.* \\)(y or n) " t match-string 1 "" 0 nil] 4])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-kill ""] 4)
#@19 Kill the program.
(defalias 'camldebug-kill #[nil "\305\212	q\210\306p!\307\310\311!\210\203 \312\fG!\204\" \313!\210\202 +@\204/ \314A!\202B \315\316\317\"\210\320\321A!\203@ \322\202A \323!)\207" [camldebug-kill-output current-camldebug-buffer camldebug-filter-function proc camldebug-filter-accumulator nil get-buffer-process camldebug-kill-filter camldebug-call "kill" zerop accept-process-output error sit-for 0 300 camldebug-call-1 y-or-n-p "y" "n"] 3 (#$ . 10843) nil])
(defalias 'camldebug-goto-filter #[(string) "	P\305\306\n\307Q\"\203 \310\311\"\312\225S\313O\305\f\"\203) \206& \314\315\305\316\"\2035 \310\317\"\315\207" [camldebug-filter-accumulator string camldebug-goto-position camldebug-goto-output comint-prompt-regexp string-match "\\(\n\\|\\`\\)[ 	]*\\([0-9]+\\)[ 	]+" "[ 	]*\\(before\\|after\\)\n" match-string 2 0 nil fail "" "\n\\(.*\\)\\'" 1] 4])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-goto ""] 4)
#@448 Go to the execution time TIME.

Without TIME, the command behaves as follows: In the camldebug buffer,
if the point at buffer end, goto time 0; otherwise, try to obtain the
time from context around point. In a caml mode buffer, try to find the
time associated in execution history with the current point location.

With a negative TIME, move that many lines backward in the camldebug
buffer, then try to obtain the time from context around point.
(defalias 'camldebug-goto #[(&optional time) "\203< \306!\211\307Y\203 \310\311\312	#\202: \313 \314\216\315\316!!\210\212\317\320\312\321\322	Z$\2033 \323\312!\2029 \324\325\322	Z\"+)\207p=\203k m\203J \307\202c \212\326\322!\210\327\330!)\203^ \331\332\322!!\202c \331\333\334!!\310\311\312#)\207\335\336 !\337`S!\312\211+,\212q\210\340p!-\341.\342\343,P!\210\203\232 \344/G!\204\242 \345-!\210\202\216 \346=?\205\271 \317\347\350,\351\260\312\321#\210\332\322!+\f\203\311 \310\311\312\331\f!#\202\320 \324\352,+#,\207" [time ntime save-selected-window-window current-camldebug-buffer address camldebug-goto-output camldebug-numeric-arg 0 camldebug-call "goto" nil selected-window ((byte-code "\301!\203\n \302!\210\301\207" [save-selected-window-window window-live-p select-window] 2)) select-window get-buffer-window re-search-backward "^Time : [0-9]+ - pc : [0-9]+ " t 1 camldebug-goto error "I don't have %d times in my history" beginning-of-line looking-at "^Time : \\([0-9]+\\) - pc : [0-9]+ " string-to-int match-string camldebug-format-command "%e" camldebug-module-name buffer-file-name int-to-string get-buffer-process camldebug-goto-filter camldebug-call-1 "info events " zerop accept-process-output fail "^Time : \\([0-9]+\\) - pc : " " - module " "$" "No time at %s at %s" camldebug-goto-position module proc camldebug-filter-function camldebug-filter-accumulator] 7 (#$ . 11872) "P"])
(defalias 'camldebug-delete-filter #[(string) "	P\306\307\310\n!\311\312\260\"\203 \313\314\"\315\225S\316O\306\"\203. \f\206+ \317\320\306\321\"\203: \313\322\"\320\207" [camldebug-filter-accumulator string camldebug-delete-file camldebug-delete-position camldebug-delete-output comint-prompt-regexp string-match "\\(\n\\|\\`\\)[ 	]*\\([0-9]+\\)[ 	]+[0-9]+[ 	]*in " regexp-quote ", character " "\n" match-string 2 0 nil fail "" "\n\\(.*\\)\\'" 1] 6])
(byte-code "\302\303\304#\210\302	\305\304#\207" [camldebug-mode-map caml-mode-map define-key "" camldebug-delete ""] 4)
#@404 Delete the breakpoint numbered ARG.

Without ARG, the command behaves as follows: In the camldebug buffer,
try to obtain the time from context around point. In a caml mode
buffer, try to find the breakpoint associated with the current point
location.

With a negative ARG, look for the -ARGth breakpoint pattern in the
camldebug buffer, then try to obtain the breakpoint info from context
around point.
(defalias 'camldebug-delete #[(&optional arg) "\2034 \306!\211\307V\203 \310\311\312	#\2022 \212\nq\210\313\314\312\315\316	Z$\203+ \317\312!\2021 \320\321\316	Z\"))\207p\n=\203q \322m\203P \212\313\312\315#\210)\323\324\316!!\202i \212\325\316!\210\326!)\203d \323\324\316!!\202i \323\327\330!!\310\311\312#*\207\327\331!\332P\327\333!\212\nq\210\334p!\335\312$%&\336\337!\210$\203\233 \340'G!\204\243 \341&!\210\202\216 $\342=\203\262 \320\343\f#\202\272 \310\311\312\323$!#.\207" [arg narg current-camldebug-buffer bpline camldebug-delete-position camldebug-delete-file camldebug-numeric-arg 0 camldebug-call "delete" nil re-search-backward "^Breakpoint [0-9]+ at [0-9]+ : file " t 1 camldebug-delete error "I don't have %d breakpoints in my history" "^Breakpoint \\([0-9]+\\) at [0-9]+ : file " string-to-int match-string beginning-of-line looking-at camldebug-format-command "%e" "%m" ".ml" "%c" get-buffer-process camldebug-delete-filter camldebug-call-1 "info break" zerop accept-process-output fail "No breakpoint in %s at %s" camldebug-delete-output camldebug-filter-function proc camldebug-filter-accumulator] 7 (#$ . 14352) "P"])
(defalias 'camldebug-complete-filter #[(string) "	P\304\305\"\203 \306\307\"\nB\310\225S\311O\202 \304\"\203+ \n\206( \312\313\304\314\"\2037 \306\315\"\313\207" [camldebug-filter-accumulator string camldebug-complete-list comint-prompt-regexp string-match "\\(\n\\|\\`\\)\\(.+\\)\n" match-string 2 0 nil fail "" "\n\\(.*\\)\\'" 1] 3])
#@62 Perform completion on the camldebug command preceding point.
(defalias 'camldebug-complete #[nil "`\212\306 \210\307	!\203 \310\225b\210`{)\311\311\312\313\n\"\210\314\315\n\"\211G\310V\2031 \n\310\nGSO\316\317\320\nP!\210\311\211\223\210\203K \321G!\204T \322\323p!!\210\202? )\324=\203] \311\325\326\"\327\f\",\207" [end comint-prompt-regexp command camldebug-complete-list command-word camldebug-filter-function beginning-of-line looking-at 0 nil string-match "\\(\\`\\| \\)\\([^ ]*\\)\\'" match-string 2 camldebug-complete-filter camldebug-call-1 "complete " zerop accept-process-output get-buffer-process fail sort string-lessp comint-dynamic-simple-complete camldebug-delete-prompt-marker camldebug-filter-accumulator] 4 (#$ . 16275) nil])
(byte-code "\304\305\306#\210\304\307\310#\210\304\311\312#\210\304	\313\314#\210\303\nB\315\303!\204$ \316\316\207" [camldebug-mode-map caml-mode-map current-load-list current-camldebug-buffer define-key "\f" camldebug-refresh "	" comint-dynamic-complete "\277" comint-dynamic-list-completions " " camldebug-break boundp nil] 4)
#@36 *Pathname for executing camldebug.
(defvar camldebug-command-name "ocamldebug" (#$ . -17382))
#@250 Run camldebug on program FILE in buffer *camldebug-FILE*.
The directory containing FILE becomes the initial working directory
and source-file directory for camldebug.  If you wish to change this, use
the camldebug commands `cd DIR' and `directory'.
(defalias 'camldebug #[(path) "\304!\305!\306\307	\310Q!\210\311!\312\313\n\"\210\314\315	P\316!\317\320\321\n	&\210\322\323p!\324\"\210\325\323p!\326\"\210\327 \210\330 )\207" [path file default-directory camldebug-command-name expand-file-name file-name-nondirectory pop-to-buffer "*camldebug-" "*" file-name-directory message "Current directory is %s" make-comint "camldebug-" substitute-in-file-name nil "-emacs" "-cd" set-process-filter get-buffer-process camldebug-filter set-process-sentinel camldebug-sentinel camldebug-mode camldebug-set-buffer] 8 (#$ . 17483) "fRun ocamldebug on file: "])
(defalias 'camldebug-set-buffer #[nil "\303=\203\n p\211\207\304 \305\216\306	!*\207" [major-mode current-camldebug-buffer save-selected-window-window camldebug-mode selected-window ((byte-code "\301!\203\n \302!\210\301\207" [save-selected-window-window window-live-p select-window] 2)) pop-to-buffer] 2])
(defalias 'camldebug-marker-filter #[(string) "	P\306\307\310\311\"\211\203A \312\313\nTTH\"?\205. \314\315\"\316\314\317\"!\314\320\"\321\230E\322\nOP\322\225\307O\307\202 \310\323\"\203Y \322\211\224OP\322\224\307O\202_ P\306*\207" [camldebug-filter-accumulator string begin output camldebug-last-frame camldebug-last-frame-displayed-p "" nil string-match "\\(H\\|M\\(.+\\):\\(.+\\):\\(before\\|after\\)\\)\n" char-equal 72 match-string 2 string-to-int 3 4 "before" 0 ".*\\'"] 5])
(defalias 'camldebug-filter #[(proc string) "\306\307\310	!!\205V \306\212\310	!q\210\311!\203# \312	!|\210\306\211\223\210\f!\205= ?\205= `\312	!Y\205= \313p!\314	\"\210)\n\205U \315 \316\216\317\n!\210\320 *))\207" [output proc process-window camldebug-delete-prompt-marker camldebug-filter-function string nil buffer-name process-buffer marker-buffer process-mark get-buffer-window comint-output-filter selected-window ((byte-code "\301!\203\n \302!\210\301\207" [save-selected-window-window window-live-p select-window] 2)) select-window camldebug-display-frame camldebug-track-frame camldebug-last-frame-displayed-p save-selected-window-window] 3])
(defalias 'camldebug-sentinel #[(proc msg) "\305\306!!\204 \307 \210\310\311\"\207\312!\313>\205O \307 \210\314\315\312!!Pp\316\216\306!q\210\317\320 !\210m\203? \321\322\f\261\210\202K \212db\210\321\322\f\261\210)\323!*\207" [proc mode-line-process obuf mode-name msg buffer-name process-buffer camldebug-remove-current-event set-process-buffer nil process-status (signal exit) ": " symbol-name ((set-buffer obuf)) set-buffer-modified-p buffer-modified-p 10 " " delete-process] 4])
#@57 Fix up a possibly garbled display, and redraw the mark.
(defalias 'camldebug-refresh #[(&optional arg) "\301 \210\302!\207" [arg camldebug-display-frame recenter] 2 (#$ . 20341) "P"])
#@194 Find, obey and delete the last filename-and-line marker from CDB.
The marker looks like \032\032FILENAME:CHARACTER\n.
Obeying it means displaying in another window the specified file and line.
(defalias 'camldebug-display-frame #[nil "\302 \210\204 \303 \210\202 \304@A@AA@#\210\305\211\207" [camldebug-last-frame camldebug-last-frame-displayed-p camldebug-set-buffer camldebug-remove-current-event camldebug-display-line t] 4 (#$ . 20533) nil])
(defalias 'camldebug-display-line #[(true-file character kind) "\306\307\310\n!\311\307\"\306\212q\210\214~\210e\f\\\312p#\210)eW\204/ dV\2034 ~\210b\210)\313\f\"-\207" [pre-display-buffer-function pop-up-windows true-file buffer window pos nil t find-file-noselect display-buffer camldebug-set-current-event set-window-point character kind] 4])
(defalias 'camldebug-remove-current-event #[nil "\203\f \304	!\210\304\n!\207\305\211\207" [window-system camldebug-overlay-event camldebug-overlay-under overlay-arrow-position delete-overlay nil] 2])
(defalias 'camldebug-set-current-event #[(pos buffer before) "\203, 	\203 \306\n\211T\f$\210\306T\307\\\f$\207\306\nS\f$\210\306\307ZS\f$\207\212\fq\210b\210\310 \210\n`\311\223\210\n\211)\207" [window-system before camldebug-overlay-event pos buffer camldebug-overlay-under move-overlay 3 beginning-of-line nil camldebug-event-marker overlay-arrow-position] 5])
(defalias 'camldebug-module-name #[(filename) "\301\302\"\303\225O\207" [filename string-match "\\([^/]*\\)\\.ml$" 1] 4])
(defalias 'camldebug-format-command #[(str) "p=?\211?\205 \n\306\203\226 \307\310\"\203\226 \311\312\224\306O!\313\224\313\225O\306\312\225\306O\314=\203L \315	\203D \316 \202F @!\202\213 \317=\203e \320	\203] \316 \202_ @!\202\213 \321=\203 \322	\203v `S\202y A@!\202\213 \323=\203\213 \324\325!\fQ+\202 \fP+\207" [current-camldebug-buffer insource camldebug-last-frame frame result str nil string-match "\\([^%]*\\)%\\([mdcep]\\)" string-to-char 2 1 109 camldebug-module-name buffer-file-name 100 file-name-directory 99 int-to-string 101 thing-at-point symbol subst cmd key] 5])
#@935 Invoke camldebug COMMAND displaying source in other window.

Certain %-escapes in FMT are interpreted specially if present.
These are:

  %m    module name of current module.
  %d    directory of current source file.
  %c    number of current character position
  %e    text of the caml variable surrounding point.

  The `current' source file is the file of the current buffer (if
we're in a caml buffer) or the source file current at the last break
or step (if we're in the camldebug buffer), and the `current' module
name is the filename stripped of any *.ml* suffixes (this assumes the
usual correspondence between module and file naming is observed).  The
`current' position is that of the current buffer (if we're in a source
file) or the position of the last break or step (if we're in the
camldebug buffer).

If ARG is present, it overrides any FMT flags and its string
representation is simply concatenated with the COMMAND.
(defalias 'camldebug-call #[(command &optional fmt arg) "\303 \210\304\305\306	\n#\"\207" [command fmt arg camldebug-set-buffer message "Command: %s" camldebug-call-1] 6 (#$ . 22686)])
(defalias 'camldebug-call-1 #[(command &optional fmt arg) "\212q\210\306\307!!b\210`\310 \210\311\n!\203 `\312\223\210*\f\203) \313\314\f!Q\2029 \2038 \315\313Q!\2029 \316\307!\317P\"\210)\207" [current-camldebug-buffer pt comint-prompt-regexp camldebug-delete-prompt-marker arg command process-mark get-buffer-process beginning-of-line looking-at nil " " int-to-string camldebug-format-command process-send-string "\n" fmt cmd] 4])
(provide 'camldebug)
